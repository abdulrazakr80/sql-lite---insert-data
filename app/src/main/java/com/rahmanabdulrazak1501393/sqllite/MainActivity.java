package com.rahmanabdulrazak1501393.sqllite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText etName;
    private EditText etPhone;
    private Button btnInsert;

    private ListView lvProfile;
    private List<String> mListProfile = new ArrayList<>();

    DatabaseHandler handler = new DatabaseHandler(this);
    ProfileModel profile = new ProfileModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        etName = (EditText) findViewById(R.id.edit_text_name);
        etPhone = (EditText) findViewById(R.id.edit_text_phone);
        btnInsert = (Button) findViewById(R.id.button_insert);
        lvProfile = (ListView) findViewById(R.id.listview_contact);



        //insert
        // lakukan validasi terlebih dahulu, jika sudah benar, maka lakukan proses insert data
        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(etName.getText().toString())){
                    etName.setError(getResources().getString(R.string.msg_cannot_allow_empty_field));
                }else if(TextUtils.isEmpty(etPhone.getText().toString())){
                    etPhone.setError(getResources().getString(R.string.msg_cannot_allow_empty_field));
                }else{
                    insertData(handler);
                }
            }
        });

        //Membaca semua data profile
        displayAllData(handler, profile);



    }


    private void displayAllData(DatabaseHandler handler, ProfileModel profile){
        mListProfile.clear();
        List<ProfileModel> list = handler.getAllDataProfile();
        for(ProfileModel profileModel : list){
            String log = "ID : " + profileModel.getId() + "\n" +
                    "NAME : " + profileModel.getName() + "\n" +
                    "ADDRESS : " + profileModel.getAddress();
            System.out.println(log);

            //add to list
            mListProfile.add(log);

            //set to the model
            profile.setId(profileModel.getId());
            profile.setName(profileModel.getName());
            profile.setAddress(profileModel.getAddress());
        }

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, mListProfile);
        lvProfile.setAdapter(arrayAdapter);
        lvProfile.invalidateViews();
        arrayAdapter.notifyDataSetChanged();
    }


    private void insertData(final DatabaseHandler databaseHandler){
        String name = etName.getText().toString();
        String phone = etPhone.getText().toString();

        ProfileModel profileModel = new ProfileModel();
        profileModel.setName(name);
        profileModel.setAddress(phone);

        databaseHandler.addProfile(profileModel);
        displayAllData(databaseHandler, profileModel);

        etPhone.setText("");
        etName.setText("");
    }



}
